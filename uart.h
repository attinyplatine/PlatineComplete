/*
 * usart.h
 *
 *  Created on: 22.05.2018
 *      Author: Fabian Spottog
 *		Im Grunde basieren die Sende- und Empfangsroutinen auf denen, welche Marc Osborne auf den zwei Seiten www.becomingmaker.com/usi-serial-uart-attiny85 (Empfangen) und www.becomingmaker.com/usi-serial-send-attiny (Senden) veröffentlicht hat. Diese Routinen habe ich in eine einzelne, asynchron arbeitende, Sende- und Empfangsroutine umgewandelt. Dabei muss sichergestellt sein, dass diese nur nacheinander arbeiten, damit nicht die aktuell verwendeten Register überschrieben werden. Auch in den Interrupt-Service-Routinen muss zuvor überprüft werden, ob derzeit Gesendet oder Empfangen wird. Die Routine arbeitet mit Interrupts und dem Timer 0, welcher ebenfalls ein Interrupt wirft.
 *		Mithilfe der Universellen Seriellen Schnittstelle (USI), welche der Attiny85 zur Verfügung stellt, habe ich das RS232 Protokoll mit TTL Pegel nachgebaut.
 */

#ifndef UART_H_
#define UART_H_
#include <stdbool.h>
#include <avr/io.h>
//Setup
//Die Baudrate mit welcher die Serielle Verbindung arbeitet.
#define        	UARTBAUDRATE    800UL
//Anzahl der Stopbits mit welcher die Serielle Verbindung arbeitet.
#define			STOPBITS		2UL
//für den Timer
#define CYCLESPERBIT ( F_CPU / UARTBAUDRATE )
//Vorberechnungen für den setup.
#if(CYCLESPERBIT > 255)
	#define DIVISOR		8UL
#else
	#define DIVISOR		1UL
#endif
#define FULLBITTICKS	( CYCLESPERBIT / DIVISOR )
#define HALFBITTICKS	( FULLBITTICKS / 2UL )
#if(FULLBITTICKS > 255)
	#error FullbitTicks sind zu groß
#endif
// Start Delay
#define STARTDELAY ( 65U + 42U )
#define TIMERSTARTDELAY ( STARTDELAY / DIVISOR )
//Definiert die verschiedenen Status, mit welchen die Statemaschine arbeitet.
enum USISERIAL_STATE	{ AVAILABLE, FIRST, SECOND };
//Muss vor der ersten Verbindung aufgerufen werden. Dies wird nicht überprüft, da es auf dem Microcontroller keine parallele Ausführung gibt. Hier wird der Timer und die Ports vorkonfiguriert.
void uartsetup(void);
//Wird regelmäßig von der Mainfunktion aufgerufen. Hier werden die Daten aus dem Buffer weiterverarbeitet, jeweils ein Byte.
void uartloop(void);
//Übergibt das letzte Zeichen aus dem Empfangsbuffer zurück und löscht dieses. Von dieser Funktion sollte nur Gebrauch gemacht werden, wenn die uartloop nicht aufgerufen wird. In der uartloop wird auf diese Funktion zurückgegriffen und falls Daten empfangen wurden, werden diese an das receive Unterprogramm weitergeleitet.
bool uartgetc(char *c);
//Sendet ein einzelnes Zeichen, bzw. fügt es dem Sendebuffer hinzu, von wo aus es anschließend versendet wird.
void uartputc(char c);
//Sendet eine Zeichenfolge. Diese Funktion zerlegt den String in seine Zeichen und greift anschließend auf die Funktion uartputc zu.
void uartputs(char *s);
//Diese Funktion wandelt eine ihr übergebene vorzeichenlose 16bit Zahl in den entsprechenden ASCII String. Intern wird mit der von Atmel stammenden Funktion utoa gearbeitet und anschließend uartputs aufgerufen.
void uartputi(uint16_t);
//Dieses Programm wartet auf das nächste empfangene Byte und gibt dieses zurück. Intern wird mit der Funktion uartgetc gearbeitet. Vorsicht ist geboten, wenn keine Daten über die Serielle Schnittstelle kommen. Es wird so lange mit einem Rücksprung gewartet bis ein Datum vorhanden ist. Außer Interrupts wird nichts abgearbeitet.
char uartgetcBlocking(void);
//Diese Funktion wird von der Interrupt Service Routine in der io.c aufgerufen, wenn es einen fallende Flanke am RX Pin gegeben hat.
void uartRxPinInterrupt(void);
#endif /* UART_H_ */
