/*
 * speicher.h
 *
 *  Created on: 14.05.2018
 *      Author: Fabian Spottog
 *      Die Kalibrierungswerte von dem Entfernungsmesser und dem Helligkeitssensor können persistent im EEPROM gespeichert werden, wo sie nach einem Neustart noch gelesenwerden können. Die dazu notwendigen Funktionen finden sich in der Header Datei avr/eeprom.h.
 */

#ifndef SPEICHER_H_
#define SPEICHER_H_
// light.

// Liest den Offset vom Helligkeitssensor.
uint16_t speicherlightreadoffset(void);
// Liest den Verstärkungsfaktor vom Helligkeitssensor.
uint16_t speicherlightreadspan(void);
// Speichert den Verstärkungsfaktor vom Helligkeitssensor.
void speicherlightwritespan(uint16_t);
// Speichert den Offset vom Helligkeitssensor.
void speicherlightwriteoffset(uint16_t);
// Entf.

// Liest den Offset vom Entfernungssensor.
uint16_t speicherentfreadoffset();
// Liest den Verstärkungsfaktor vom Entfernungssensor.
uint16_t speicherentfreadspan();
// Speichert den Offset vom Entfernungssensor.
void speicherentfwritespan(uint16_t);
// Speichert den Verstärkungsfaktor vom Entfernungssensor.
void speicherentfwriteoffset(uint16_t);


#endif /* SPEICHER_H_ */
