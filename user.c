/*
 * user.h
 *
 *  Created on: 09.06.2018
 *      Author: Fabian Spottog
 *      Hier befindet sich das eigentliche Programm. Es wird auf die verschiedenen Programmteile zugegriffen und alles zu einem funktionierenden und interagierenden Programm vereint. Es stehen mehrere Funktionen zur Verfügung, welche zu unterschiedlichen Zeitpunkten aufgerufen werden.

 */
#include <avr/io.h>
#include <stdbool.h>
#include "io.h"
#include "light.h"
#include "entf.h"
#include "uart.h"
#include "helper.h"
#include "user.h"
#include "fifo/fifo.h"


// Wird aufgerufen, nachdem alle anderen Setuproutinen aufgerufen wurden, aber noch bevor die Mainloop beginnt. Hier setze ich die Kalibrierungswerte, schalte das Binding der LED an den Taster ein. Schalte die LED als Bereitschaftsanzeige an und gebe die Begrüßung über die Serielle Schnittstelle aus.
void usersetup(void){
	// Resete span und offset, sonst gibt es bei neuen Controllern Probleme
	lightSetSpan(1);
	lightSetOffset(0);
	entfSetSpan(1);
	entfSetOffset(0);
	// Aktiviere die Interrupt gesteuerte Funktion, dass die LED Syncron zum Taster geschaltet wird.
	ioSetBindTasterToLed(true);
	// Signalisiere Bereitschaft
	ioSetLed(true);
	uartputs("Hallo\n");
}
// Diese Funktion wird bei jedem Durchlauf der Mainloop aufgerufen. Ich habe hier keine Funktionen implementiert.
void userloop(void){

}
// Diese Funktion wird bei jedem 16384. Durchlauf der Main Funktion aufgerufen. Ich sende hier den Zustand aller Sensoren über die Serielle Schnittstelle.
void userslowloop(void) {
	//Taster Output
	uartputs("Taster ");
	if (!ioGetTaster())uartputs("nicht ");
	uartputs("gedrückt\n");
	// Entfernungsmesser Output
	uartputs("Entfernung ");
	uartputi(entfGet());
	uartputc('\n');
	// Helligkeitssensor Output
	uartputs("Helligkeit ");
	uartputi(lightGet());
	uartputc('\n');
}

