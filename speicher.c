/*
 * hallo.c
 *
 *  Created on: 23.03.2018
 *      Author: Fabian Spottog
 *      Die Kalibrierungswerte von dem Entfernungsmesser und dem Helligkeitssensor können persistent im EEPROM gespeichert werden, wo sie nach einem Neustart noch gelesenwerden können. Die dazu notwendigen Funktionen finden sich in der Header Datei avr/eeprom.h.
 */

#include <avr/io.h>
#include <avr/eeprom.h>
#include "speicher.h"


// Adressen deklarieren.
uint16_t lightspan EEMEM = 100;
uint16_t lightoffset EEMEM = 101;
uint16_t entfspan EEMEM = 102;
uint16_t entfoffset EEMEM = 103;

// Liest den Offset vom Helligkeitssensor.
uint16_t speicherlightreadoffset(void){
	return eeprom_read_word(&lightoffset);
}
// Liest den Verstärkungsfaktor vom Helligkeitssensor.
uint16_t speicherlightreadspan(void){
	return eeprom_read_word(&lightspan);
}
// Speichert den Offset vom Helligkeitssensor.
void speicherlightwriteoffset(uint16_t newoffset){
	eeprom_update_word(&lightoffset, newoffset);
}
// Speichert den Verstärkungsfaktor vom Helligkeitssensor.
void speicherlightwritespan(uint16_t newspan){
	eeprom_update_word(&lightspan, newspan);
}
// Liest den Offset vom Entfernungssensor.
uint16_t speicherentfreadoffset(){
	return eeprom_read_word(&entfoffset);
}
// Liest den Verstärkungsfaktor vom Entfernungssensor.
uint16_t speicherentfreadspan(){
	return eeprom_read_word(&entfspan);
}
// Speichert den Offset vom Entfernungssensor.
void speicherentfwriteoffset(uint16_t newoffset){
	eeprom_update_word(&entfoffset, newoffset);
}
// Speichert den Verstärkungsfaktor vom Entfernungssensor.
void speicherentfwritespan(uint16_t newspan){
	eeprom_update_word(&entfspan, newspan);
}
