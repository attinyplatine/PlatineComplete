/*
 * hallo.c
 *
 *  Created on: 23.03.2018
 *      Author: Fabian Spottog
 *      Mit Hilfe des Analog Digital Wandlers wird die analoge Spannung gemessen welche dem Helligkeitswert entspricht. Der im Attiny integrierte ADC wird im 10 Bit Free Run Modus mit einem Vorteiler von 8 betrieben. Das bedeutet, alle acht Taktzyklen wird automatisch eine Messung durchgeführt. Das sind 125.000 Messungen (125 KHz) in der Sekunde oder eine Messung dauert 87μs. Im Datenblatt wird eine Messdauer von 65-260μs empfohlen. Ist die Messdauer zu gering, kann es sein, dass die Sample & Hold Schaltung nicht schnell genug eingestellt hat. Ist die Messdauer zu lang, kann die Sample & Hold Schaltung die Spannung nicht lange genug halten (siehe Elektronik Grundlagen). Nach dem erfolgreichen Abschluss einer Messung wird diese in das ADC Register geschrieben. Von diesem Register aus liest die Software die Messung, sobald der Messwert angefordert wird. Eine ISR Routine oder eine Aktion in der Loop Funktion wird nicht benötigt. Die komplette Arbeit macht der ADC in Hardware.
 */

#ifndef LIGHT_H_
#define LIGHT_H_
	//Der Aufruf erfolgt aus der Main Methode. Hier wird der ADC konfiguriert, die Abgleichwerte aus dem EEPROM gelesen und auf Plausibilität geprüft.
	void lightsetup(void);
	//Die Loop Funktion ist nur zur Vollständigkeit implementiert.
	void lightloop(void);
	//Liest den zuletzt gemessenen Wert direkt aus dem ADC Register und gibt ihn zurück.
	uint16_t lightGetADCValue(void);
	//Gibt den zuletzt gemessen Wert korrigiert um die Kalibrierungswerte zurück.
	uint16_t lightGet(void);
	//Speichert den übergebenen Offsetwert und kopiert ihn ins EEPROM.
	void lightSetOffset(uint16_t);
	//Speichert den übergebenen Verstärkungsfaktor und kopiert ihn ins EEPROM
	void lightSetSpan(uint16_t);
	//Gibt den aktuellen Offsetwert zurück.
	uint16_t lightGetOffset(void);
	//Gibt den aktuellen Verstärkungsfaktor zurück.
	uint16_t lightGetSpan(void);
#endif /* LIGHT_H_ */
