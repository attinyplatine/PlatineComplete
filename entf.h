/*
 * entf.h
 *
 *  Created on: 14.05.2018
 *      Author: Fabian Spottog
 *		Der Entfernungsmesser liefert die Entfernung anhand der Länge des High Signales an den Microcontroller. Es muss also die zeitliche Differenz zwischen der Steigenden und der Fallenden Flanke gemessen werden. Dies habe ich mit Hilfe des Timers1, welcher ein Interrupt bei Überlauf liefert und den Pin Chance Interrupt aus der io.c Datei implementiert.
 *		Der Timer1 ist ein 8 Bit Timer. Eine Erweiterung auf 16 Bit ist unter der zu Hilfenahme des Overflow Interrupts in Software möglich.
 *		Zur Kalibrierung ist ein Offsetwert und ein Verstärkungswert in dem EEPROM gespeichert.
 *		Zur besseren Veranschaulichung ist der Ablaufplan pollend gezeichnet, obwohl mit Interrupts gearbeitet wird.
 */

#ifndef ENTF_H_
#define ENTF_H_
	//Diese Funktion wird beim Starten von der Main Funktion aufgerufen.Als Erstes wird der Timer initialisiert. Anschließend werden aus dem EEPROM die gespeicherten Kalibrierungswerte gelesen und auf Plausibilität geprüft. Nach dem Durchlaufen wird die Freigabe für die Interrupts gesetzt.
	void entfsetup(void);
	// Die Loop Methode ist nur der Vollständigkeit halber implementiert und wird von der Main Funktion aufgerufen.
	void entfloop(void);
	// Gibt den Wert der letzten Messung ohne Anwendung der Kalibrierungswerte zurück.
	uint16_t entfGetCountValue(void);
	// Gibt den Wert der letzten Messung unter Anwendung der Kalibrierungswerte zurück.
	uint16_t entfGet(void);
	// Setzt einen neuen Offsetwert und speichert diesen in das EEPROM.
	void entfSetOffset(uint16_t);
	// Setzt einen neuen Verstärkungsfaktor und speichert diesen in das EEPROM.
	void entfSetSpan(uint16_t);
	// Diese Interrupt Routine wird vom Timer1 aufgerufen, wenn dieser überläuft. Dies erweitert den Timer1 von 8 Bit auf 16 Bit.
	void entfPingLowInterrupt(void);
	// Steigende Flanke des Pins. Diese Funktion wird nach einem Pin Chance Interrupt von der io.c aufgerufen.
	void entfPingHighInterrupt(void);
#endif /* ENTF_H_ */
