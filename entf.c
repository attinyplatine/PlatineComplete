/*
 * hallo.c
 *
 *  Created on: 23.03.2018
 *      Author: Fabian Spottog
 *		Der Entfernungsmesser liefert die Entfernung anhand der Länge des High Signales an den Microcontroller. Es muss also die zeitliche Differenz zwischen der Steigenden und der Fallenden Flanke gemessen werden. Dies habe ich mit Hilfe des Timers1, welcher ein Interrupt bei Überlauf liefert und den Pin Chance Interrupt aus der io.c Datei implementiert.
 *		Der Timer1 ist ein 8 Bit Timer. Eine Erweiterung auf 16 Bit ist unter der zu Hilfenahme des Overflow Interrupts in Software möglich.
 *		Zur Kalibrierung ist ein Offsetwert und ein Verstärkungswert in dem EEPROM gespeichert.
 *		Zur besseren Veranschaulichung ist der Ablaufplan pollend gezeichnet, obwohl mit Interrupts gearbeitet wird.
 */
#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdbool.h>
#include "entf.h"
#include "speicher.h"

volatile uint16_t entfCountValue;		// nach einem vollständigen Impuls wird hier der Wert eingefügt
volatile uint8_t countOverflows;		// Anzahl der Bufferüberläufe. Dieser Wert wird immer verändert und ist nie gültig. Wenn gültig wird auf 0 gesetzt.
//Offset zur Berechnung der Entfernung.
uint16_t Offset;
// Verstärkung zur Berechnung der Entfernung.
uint16_t Span;
// Init
volatile bool SetupComplete = false;
// Diese Funktion wird beim Starten von der Main Funktion aufgerufen.
// Als Erstes wird der Timer initialisiert. Anschließend werden aus dem EEPROM die gespeicherten Kalibrierungswerte gelesen und auf Plausibilität geprüft. // Nach dem Durchlaufen wird die Freigabe für die Interrupts gesetzt.
void entfsetup() {
	//init Timer.
	TIMSK |= (1<<TOIE1);					// Overflow Interrupt aktivieren.
	TCCR1 = (1<<CS13)|(0<<CS12)|(0<<CS11)|(0<<CS10); // Takt teiler

	//Offset und Span Lesen
	Offset = speicherentfreadoffset();
	Span = speicherentfreadspan();
	// Wenn Span 0 dann auf 1 setzen.
	// So wird garantiert, dass etwas gelesen werden kann.
	if(!Span) entfSetSpan(1);
	SetupComplete = true;
}
//Die Loop Methode ist nur der Vollständigkeit halber implementiert und wird von der Main Funktion aufgerufen.
void entfloop() {
}
// Gibt den Wert der letzten Messung ohne Anwendung der Kalibrierungswerte zurück.
uint16_t	entfGetCountValue(){
	return entfCountValue;
}
// Gibt den Wert der letzten Messung unter Anwendung der Kalibrierungswerte zurück.
uint16_t entfGet(){
	return (entfGetCountValue() + Offset) * ((uint16_t)Span);
}
// Setzt einen neuen Offsetwert und speichert diesen in das EEPROM.
void entfSetOffset(uint16_t newOffset){
	Offset = newOffset;
	speicherentfwriteoffset(newOffset);
}
// Setzt einen neuen Verstärkungsfaktor und speichert diesen in das EEPROM.
void entfSetSpan(uint16_t newSpan){
	Span = newSpan;
	speicherentfwritespan(newSpan);
}
// Diese Interrupt Routine wird vom Timer1 aufgerufen, wenn dieser überläuft. Dies erweitert den Timer1 von 8 Bit auf 16 Bit.
ISR(TIMER1_OVF_vect){
	++countOverflows;
}
// Steigende Flanke des Pins. Diese Funktion wird nach einem Pin Chance Interrupt von der io.c aufgerufen.
// Es wird der Zähler zurückgesetzt.
void entfPingHighInterrupt() {
	if(SetupComplete){
		// High also Beginn des Pulses.
		TCNT1 = 0;						// Zähler zurücksetzen
		countOverflows = 0;				// Overflow Zähler zurücksetzen
		TIFR = (1 << TOV1);	// Wenn vorhanden Interrupt Flag für den Zählerüberlauf löschen.
	}
}
// Es wird geprüft ob ein noch nicht verarbeiteter Timer Interrupt auf seine Bearbeitung wartet und dieser dann durchgeführt. Anschließend wird das Ergebnis errechnet und gespeichert.
void entfPingLowInterrupt() {
	if(SetupComplete){
		// Low Signal also Ende der Messung.
		if (TIFR & (1 << TOV1)) {//Es wartet noch ein Zählerüberlauf Interrupt auf die Ausführung dann verarbeiten.
			++countOverflows;
			TIFR = (1 << TOV1);	//Interrupt Flag löschen.
		}
		entfCountValue = (255 * countOverflows) + TCNT1;	// Berechne den Wert.
	}
}
