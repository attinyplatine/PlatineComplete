################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../entf.c \
../helper.c \
../io.c \
../light.c \
../main.c \
../receive.c \
../speicher.c \
../uart.c \
../user.c 

OBJS += \
./entf.o \
./helper.o \
./io.o \
./light.o \
./main.o \
./receive.o \
./speicher.o \
./uart.o \
./user.o 

C_DEPS += \
./entf.d \
./helper.d \
./io.d \
./light.d \
./main.d \
./receive.d \
./speicher.d \
./uart.d \
./user.d 


# Each subdirectory must supply rules for building sources it contributes
%.o: ../%.c
	@echo 'Building file: $<'
	@echo 'Invoking: AVR Compiler'
	avr-gcc -Wall -Os -fpack-struct -fshort-enums -ffunction-sections -fdata-sections -std=gnu99 -funsigned-char -funsigned-bitfields -mmcu=attiny85 -DF_CPU=1000000UL -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


