/*
 * receive.c
 *
 *  Created on: 11.06.2018
 *      Author: Fabian Spottog
  *      Wenn die Serielle Schnittstelle, die empfangenen Daten in den Buffer geschrieben hat, werden diese von dort byteweise an den Programmcode in dem receive Unterprogramm zur Verarbeitung übergeben.
 *      Das Unterprogramm verfügt über einen kleinen 16 Byte großen Zwischenbuffer, der Platz für immer genau einen Befehl bieten muss. Sobald ein Ende Zeichen empfangen wurde, führt das Programm den gesamten im Buffer stehenden Befehl aus. Anschließend wir der Buffer gelöscht.
 */
#include <avr/io.h>
#include <string.h>
#include "receive.h"
#include <stdbool.h>
#include "io.h"

#define BUFFERSIZE 16

char data[BUFFERSIZE];
uint8_t  pointer = 0;
// Zähler zurücksetzen und alles mit Ende-Kennung überschreiben
void delete(){
	pointer = 0;
	for(int i=0; i< BUFFERSIZE; i++){
		data[i] = '\0';
	}
}
// Hier wird der Buffer initialisiert.
void receiveSetup(){
	delete();
}
// // Prüft ob ein Ende Zeichen (\0 oder \n) empfangen wurde. Wurde das Ende Zeichen erkannt wird der komplette Buffer an die unten beschriebene Funktion weitergegeben. Wenn kein Ende Zeichen erkannt wurde, wird es dem Buffer
void receiveAdd(char c){
	if(('\n' == c) | ('\0' == c)){
		receive(data);
		delete();
	}else{
		data[pointer++] = c;
		// damit auf keinen Fall fremder Speicherbereich überschrieben wird.
		if(BUFFERSIZE == pointer)
			delete();
	}
}
// Hier wird der Inhalt des Buffers mit den programmierten Funktionen verglichen und die entsprechenden Funktionen aufgerufen.
void receive(char* input){
	if((strncmp(input, "led an", 6)==0) || (strncmp(input, "led on", 6)==0)){
		ioSetLed(true);
	}else if((strncmp(input, "led aus", 7)==0) || (strncmp(input, "led off", 7)==0)){
		ioSetLed(false);
	}else if(strncmp(input, "led", 3)==0){
		ioToggleLed();
	}
}


