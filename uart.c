/*
 * usart.c
 *
 *  Created on: 23.05.2018
 *      Author: Fabian Spottog
 *		Im Grunde basieren die Sende- und Empfangsroutinen auf denen, welche Marc Osborne auf den zwei Seiten www.becomingmaker.com/usi-serial-uart-attiny85 (Empfangen) und www.becomingmaker.com/usi-serial-send-attiny (Senden) veröffentlicht hat. Diese Routinen habe ich in eine einzelne, asynchron arbeitende, Sende- und Empfangsroutine umgewandelt. Dabei muss sichergestellt sein, dass diese nur nacheinander arbeiten, damit nicht die aktuell verwendeten Register überschrieben werden. Auch in den Interrupt-Service-Routinen muss zuvor überprüft werden, ob derzeit Gesendet oder Empfangen wird. Die Routine arbeitet mit Interrupts und dem Timer 0, welcher ebenfalls ein Interrupt wirft.
 *		Mithilfe der Universellen Seriellen Schnittstelle (USI), welche der Attiny85 zur Verfügung stellt, habe ich das RS232 Protokoll mit TTL Pegel 
 */

#include "uart.h"

#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include "helper.h"
#include "fifo/fifo.h"
#include "receive.h"

#if ! defined(FULLBITTICKS)
	#error "NO FULLBITTICKS DEFINED!"
#else
	#pragma message "FULLBITTICKS is defined with: " STR(FULLBITTICKS)
#endif
static volatile uint8_t usiserial_tx_data = 0;
static volatile uint8_t ZaehlerBisSenden = 0;
static volatile enum USISERIAL_STATE usiserial_tx_state = AVAILABLE;
static volatile enum USISERIAL_STATE usiserial_rx_state = AVAILABLE;
static volatile bool warnungGesendet = false;
//Muss vor der ersten Verbindung aufgerufen werden. Dies wird nicht überprüft, da es auf dem Microcontroller keine parallele Ausführung gibt. Hier wird der Timer und die Ports vorkonfiguriert.
void uartsetup() {
	DDRB |=	 (1 << PB1);					// Pin PB1 aus Ausgang konfigurieren.
	PORTB |= 1 << PB1;						// Port auf High setzen.
	//DDRB &= ~(1 << DDB0);					// Pin DB0 als Eingang festlegen.						// wird in io.c gemacht
	PORTB |= 1 << PB0;						// Internen Pull-up Widerstand an PB0 aktivieren.
	USICR = 0;								// Disable USI.
	//GIMSK |= 1 << PCIE;						// Interrupt für Pegeländerungen einschalten 		// wird in io.c gemacht
	//PCMSK |= 1 << PCINT0;					// PB9 Interrupt einschalten.							// wird in io.c gemacht
	// Timer0 konfigurieren.
	TCCR0A = 1<<WGM01; // Dadurch wird nur WGM01 gesetzt	// CTC MODE
	if(DIVISOR > 1) TCCR0B |= (1 << CS01); else TCCR0B |= (1 << CS00);// prescale auf cpu clk oder clk/8
}
//Diese Funktion wird von der Interrupt Service Routine in der io.c aufgerufen, wenn es einen fallende Flanke am RX Pin gegeben hat.
//ISR(PCINT0_vect){
void uartRxPinInterrupt(){
	//wird nicht über Interrupt gemacht, da diese global verarbeitet werden
	//Nur Empfangen wenn nicht gesendet wird
	if((AVAILABLE == usiserial_tx_state) && (AVAILABLE == usiserial_rx_state)){
		usiserial_rx_state = FIRST;
		ZaehlerBisSenden = 255;
		//GIMSK &= ~( 1 << PCIE );			// Interrupt bei Pegel Änderung ausschalten
		// Timer0 konfigurieren.
		GTCCR |= 1 <<PSR0;					// Reset Prescaler
		TCNT0 = 0;							// ab der 0 hochzählen.
		OCR0A = (uint8_t)(HALFBITTICKS - TIMERSTARTDELAY);//startdelay berechnen.
		//Starten
		TIFR = 1 << OCF0A;					//Alte Interuptflags löschen
		TIMSK |= 1 << OCIE0A;				//Timer Interrups einschalten.
		warnungGesendet = false;
	}else if((AVAILABLE == usiserial_rx_state) && (!warnungGesendet)){
		// Warnung wird nur einmal versendet sonst wird die Sende Fifo geflutet bei zu vielen empfangenen Daten.
		warnungGesendet = true;
		uartputs("sorry Halbduplex.\n");
	}
}
//Timer Interrupt wird ausgelöst, damit das nächste Bit gelesen werden kann.
ISR(TIMER0_COMPA_vect){
	if((AVAILABLE == usiserial_tx_state) && (FIRST == usiserial_rx_state)){
		usiserial_rx_state = SECOND;
		// Empfange
		// Timer für den nächsten Zeitraum vorbereiten
		TCNT0 = 0;								// bei 0 weiterzählen
		OCR0A = FULLBITTICKS;					// dann müsste das nächste Bit soweit sein.
		//Universal Serial Interface konfigurieren.
		USICR = 1<<USIOIE | 0<<USIWM0 | 1<<USICS0;
		USISR = 1<<USIOIF | 8;
	}
}
//Interrupt des USI wenn fertig
ISR(USI_OVF_vect){
	switch(usiserial_tx_state){
	case FIRST:{
		usiserial_tx_state = SECOND;
		USIDR =  (usiserial_tx_data << 7) | 0x7f;	// die letzten Bits + Stopbits ins USI Ausgangsregister schieben.
		USISR = (1<<USIOIF) | (16-(1+(STOPBITS)));	// Anzahl der Bits festlegen.
		break;
	}
	case SECOND:{
		PORTB |= 1 << PB1;						// Sicherstellen, dass der Pin High ist.
		DDRB |= (1<<PB1);
		USICR = 0;								// USI Debugs deaktivieren.
		USISR |= 1<<USIOIF;						/// Interrupt Flag löschen
		usiserial_tx_state = AVAILABLE;
		break;
	}
	default:{	//am Empfangen.
		fifoRxAdd(USIDR);						// Empfangenes Byte in dem Buffer sichern.
		USICR = 0;								// Register löschen
		GIFR = 1<<PCIF;							// Interrupt Flag für Pegeländerung löschen.
		USISR |= 1<<USIOIF;						// Interrupt Flag löschen
		usiserial_rx_state = AVAILABLE;			// wieder freigeben
		break;
	}}

}
// Sende ein Byte
void uartputbyte(char data){
	while((AVAILABLE != usiserial_rx_state) || (AVAILABLE != usiserial_tx_state));	//Warte bis senden fertig ist, und nichts empfangen wird.
	usiserial_tx_state = FIRST;
	TIFR = 1 << OCF0A;						//Alte Interuptflags löschen
	usiserial_tx_data = ReverseAllBits(ReverseByte((uint8_t)data));
	OCR0A = FULLBITTICKS;					// Zeit einstellen.
	GTCCR |= 1 <<PSR0;						// Reset Prescaler
	TCNT0 = 0;								// ab der 0 hochzählen.
	// USI Modul zum Senden einrichten.
	USICR = (0<<USIWM1)|(1<<USIWM0)|(1<<USIOIE)|(0<<USICS1)|(1<<USICS0)|(0<<USICLK);
	USIDR = (usiserial_tx_data >> 1);			//Startbit und die ersten 7 Bit ins USI Datenregister laden.
	USISR = (1<<USIOIF) | (16-8);				// Usi Overflow Interrupt löschen und auf 8 Bits festlegen.
}

//Wird regelmäßig von der Mainfunktion aufgerufen. Hier werden die Daten aus dem Buffer weiterverarbeitet, jeweils ein Byte.
void uartloop(void) {
	char tmp = 0;
	// Wenn derzeit nicht gesendet und nicht empfangen wird, schiebe das nächste Zeichen von dem Fifo Buffer in den Ausgangsbuffer.
	if((AVAILABLE == usiserial_rx_state) && (AVAILABLE == usiserial_tx_state)){
				// Bereit zum Senden. Prüfe ob Daten vorliegen, welche versendet werden können.
		if(ZaehlerBisSenden == 0 && fifoTxGet(&tmp))
		{
			uartputbyte(tmp);
		}else{
			if(ZaehlerBisSenden > 0)
				ZaehlerBisSenden--;
		}
	}
	if(fifoRxGet(&tmp)){
		receiveAdd(tmp);
	}
}
//Sendet eine Zeichenfolge. Diese Funktion zerlegt den String in seine Zeichen und greift anschließend auf die Funktion uartputc zu.
void uartputs(char *s){
	while(*s){ 			// Solange keine 0
		uartputc(*s);	// Sende das Zeichen
		s++;			// nächste Zeichen
	}
}
//Diese Funktion wandelt eine ihr übergebene vorzeichenlose 16bit Zahl in den entsprechenden ASCII String. Intern wird mit der von Atmel stammenden Funktion utoa gearbeitet und anschließend uartputs aufgerufen.
void uartputi(uint16_t zahl){
	char buf[8* sizeof(unsigned int)];
	utoa(zahl, buf, 10);
	uartputs(buf);
}
//  Fassade, weil in die Fifo weitergeschleift wird.

//Sendet ein einzelnes Zeichen, bzw. fügt es dem Sendebuffer hinzu, von wo aus es anschließend versendet wird.
void uartputc(char c){	fifoTxAdd(c);}
//Übergibt das letzte Zeichen aus dem Empfangsbuffer zurück und löscht dieses. Von dieser Funktion sollte nur Gebrauch gemacht werden, wenn die uartloop nicht aufgerufen wird. In der uartloop wird auf diese Funktion zurückgegriffen und falls Daten empfangen wurden, werden diese an das receive Unterprogramm weitergeleitet.
bool uartgetc(char *c) { return fifoRxGet(c); }
//Dieses Programm wartet auf das nächste empfangene Byte und gibt dieses zurück. Intern wird mit der Funktion uartgetc gearbeitet. Vorsicht ist geboten, wenn keine Daten über die Serielle Schnittstelle kommen. Es wird so lange mit einem Rücksprung gewartet bis ein Datum vorhanden ist. Außer Interrupts wird nichts abgearbeitet.
char uartgetcBlocking(){ char c; while(!uartgetc(&c)); return c; }



