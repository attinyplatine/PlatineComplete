/*
 * user.h
 *
 *  Created on: 09.06.2018
 *      Author: Fabian Spottog
 *      Hier befindet sich das eigentliche Programm. Es wird auf die verschiedenen Programmteile zugegriffen und alles zu einem funktionierenden und interagierenden Programm vereint. Es stehen mehrere Funktionen zur Verfügung, welche zu unterschiedlichen Zeitpunkten aufgerufen werden.
 */

#ifndef USER_H_
// Wird aufgerufen, nachdem alle anderen Setuproutinen aufgerufen wurden, aber noch bevor die Mainloop beginnt. Hier setze ich die Kalibrierungswerte, schalte das Binding der LED an den Taster ein. Schalte die LED als Bereitschaftsanzeige an und gebe die Begrüßung über die Serielle Schnittstelle aus.
void usersetup(void);
// Diese Funktion wird bei jedem Durchlauf der Mainloop aufgerufen. Ich habe hier keine Funktionen implementiert.
void userloop(void);
// Diese Funktion wird bei jedem 16384. Durchlauf der Main Funktion aufgerufen. Ich sende hier den Zustand aller Sensoren über die Serielle Schnittstelle.
void userslowloop(void);
#define USER_H_



#endif /* USER_H_ */
