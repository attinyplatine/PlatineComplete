/*
 * hallo.c
 *
 *  Created on: 23.03.2018
 *      Author: Fabian Spottog
 *       Der Programmcode für die digitalen Ein- bzw. Ausgänge sind in der io.c. Hier befindet sich auch der Code für die Interrupts für den RX Pin und der Eingang für den Entfernungsmesser, da diese sich eine gemeinsame Interrupt Service Routine teilen.
 */
#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdbool.h>
#include "io.h"
#include "uart.h"	//damit die Interrupt Routinen gefunden werden
#include "entf.h"	//damit die Interrupt Routinen gefunden werden

// Konstanten
volatile bool SetBindTasterToLed = false;
// Init
static volatile uint8_t pinInterruptMask; // Zwischenspeicher für die Pin Interrupt maske
// In der von der Main Setup Methode aufgerufenen Funktion, werden die Ausgänge als Ausgänge konfiguriert (der Standardwert ist eine Konfiguration als Eingang). Eingänge werden die internen Pullup- Widerstände eingeschaltet und die Interrupts
void iosetup() {
	//Setze den Pin B2 auf dem PortB als Ausgang.
	DDRB |= 1 << DDB2;
	PORTB |= (1 << DDB4) | (1<<DDB5)| (1<<DDB0);	//Aktiviere Pull-up Widerstände
	// Aktiviere die Pin Value Change Intterupts, diese werden an die entf.c und uart.c je nach Pin durchgeschleift.
	pinInterruptMask = PINB & ((1<<DDB0)|(1<<DDB3)|(1<<DDB4));
	GIMSK |= 1 << PCIE;						// Interrupt für Pegeländerungen einschalten
	PCMSK |= (1 << PCINT0)|(1 << PCINT3)|(1 << PCINT4);// Interrupts einschalten für folgende Pins=0=UART RX, 3=Taster, 4= Entfernungsmesser
}

// Die Loop Methode gibt es nur der Vollständigkeit halber.
void ioloop() {
}


// LED bzw Outputs

// Setzt den Ausgang der LED auf den übergebenen Wert und gibt diesen zurück.
bool ioSetLed(bool neuerWert) {
	if (neuerWert) {
		// setze Ausgang auf High
		PORTB |= 1 << DDB2;
	} else {
		// setze Ausgang auf Low
		PORTB &= ~(1 << DDB2);
	}
	return neuerWert;
}

// Taster bzw. Input

// Gibt den aktuellen Zustand der LED zurück.
bool ioGetLed(){
	return PINB & (1 << DDB2);
}

//bindings

// Wenn die LED an den Taster gebunden wird, wird der Ausgang bei einem Taster Interrupt auf den selben Wert gesetzt. Dieses Binden kann mit der Funktion Ein- bzw. Ausgeschaltet werden.
void ioSetBindTasterToLed(bool binding){
	SetBindTasterToLed = binding;
}

// Mit dieser Funktion wird der Zustand des Binden der LED an den Taster abgefragt.
bool ioGetBindTasterToLed(){
	return SetBindTasterToLed;
}
// Diese Funktion setzt die LED auf den Zustand des Tasters.
bool ioSetLedToTaster(){
	return ioSetLed(ioGetTaster());
}
// Diese Funktion gibt den Zustand des Tasters zurück.
bool ioGetTaster(){
	return 0==(PINB & (1 << DDB4));
}
// fürs binding
void TasterInterrupt(){
	if(ioGetBindTasterToLed()){
		//Setze den Ausgang auf den aktuellen Zustand des Einganges.
		ioSetLedToTaster();
	}
}
// Wenn die LED Aus geschaltet ist, wird sie Ein geschaltet. Wenn die LED Ein geschaltet ist, wird sie Aus geschaltet.
bool ioToggleLed(void) {
	//PORTB ^= (1 << DDB2); Wäre besser aber so ist es verständlicher.
	return ioSetLed(!ioGetLed());
}
// Das ist die Interrupt Service Routine des Pin Chance Interrupts. Diese wird von dem Microcontroller aufgerufen, wenn sich einer der Pins PB0 (uart RX), PB2 (Taster) oder PB4 (Entfernungsmesser) ändert. Ein Aufruf von der Software ist nicht vorgesehen, auch wenn es keine negativen Folgen hätte. In der ISR wird geprüft, welcher Pin sich seit dem letzten Aufruf geändert hat und die entsprechenden Funktionen in den Unterprogrammen aufgerufen.
// Reinfolge nach Priorität.
// Zeitmessung ist Zeitkritischer als so ein Lamer Taster.
ISR(PCINT0_vect){
	uint8_t port = PINB; // Zwischenspeichern damit sich der Wert nicht Ändern kann.
	if((pinInterruptMask & (1<<DDB3)) != (port & (1<<DDB3))){		// Entfernungsmesser
		if(port & (1<<DDB3)){
			pinInterruptMask |= (1<<DDB3);
			entfPingHighInterrupt();
		}else{
			pinInterruptMask &= ~(1<<DDB3);
			entfPingLowInterrupt();
		}
	}
	if((pinInterruptMask & (1<<DDB0)) != (port & (1<<DDB0))){		// Uart RX
		if(port & (1<<DDB0)){// Pin High wird nicht Ausgewertet.
			pinInterruptMask |= (1<<DDB0);
		}else{
			pinInterruptMask &= ~(1<<DDB0);
			uartRxPinInterrupt();
		}
	}
	if((pinInterruptMask & (1<<DDB4)) != (port & (1<<DDB4))){		//Taster
		if(port & (1<<DDB4)){
			pinInterruptMask |= (1<<DDB4);
		}else{
			pinInterruptMask &= ~(1<<DDB4);
		}
		TasterInterrupt();
	}
}
