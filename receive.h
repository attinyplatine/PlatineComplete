/*
 * receive.h
 *
 *  Created on: 11.06.2018
 *      Author: Fabian Spottog
 *      Wenn die Serielle Schnittstelle, die empfangenen Daten in den Buffer geschrieben hat, werden diese von dort byteweise an den Programmcode in dem receive Unterprogramm zur Verarbeitung übergeben.
 *      Das Unterprogramm verfügt über einen kleinen 16 Byte großen Zwischenbuffer, der Platz für immer genau einen Befehl bieten muss. Sobald ein Ende Zeichen empfangen wurde, führt das Programm den gesamten im Buffer stehenden Befehl aus. Anschließend wir der Buffer gelöscht.
 */

 #ifndef RECEIVE_H

// Prüft ob ein Ende Zeichen (\0 oder \n) empfangen wurde. Wurde das Ende Zeichen erkannt wird der komplette Buffer an die unten beschriebene Funktion weitergegeben. Wenn kein Ende Zeichen erkannt wurde, wird es dem Buffer
void receiveAdd(char c);
//Hier wird der Buffer initialisiert.
void receiveSetup(void);
// Hier wird der Inhalt des Buffers mit den programmierten Funktionen verglichen und die entsprechenden Funktionen aufgerufen.
void receive(char* input);


#define RECEIVE_H
#endif /* RECEIVE_H */