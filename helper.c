/*
 * helper.c
 *
 *  Created on: 05.06.2018
 *      Author: Fabian Spottog
 *              Hilfsfunktionen
 */
#include <avr/io.h>
#include "helper.h"


// Wie bei der selben Funktion für den Linux PC werden hier die Bits innerhalb von
uint8_t ReverseByte(uint8_t byte){
	byte = ((byte>>1)&0x55) | ((byte<<1)&0xAA);
	byte = ((byte>>2)&0x33) | ((byte<<2)&0xCC);
	byte = ((byte>>4)&0x0F) | ((byte<<4)&0xF0);
	return byte;
}
// Hier wird jedes Bit in einem Byte gekippt, z.B.: aus 00110010 wird 11001101.
uint8_t ReverseAllBits(uint8_t byte){
	return 0xFF ^ byte;
}
