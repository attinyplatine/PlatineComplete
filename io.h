/*
 * io.h
 *
 *  Created on: 14.05.2018
 *      Author: Fabian Spottog
 /      Der Programmcode für die digitalen Ein- bzw. Ausgänge sind in der io.c. Hier befindet sich auch der Code für die Interrupts für den RX Pin und der Eingang für den Entfernungsmesser, da diese sich eine gemeinsame Interrupt Service Routine teilen.
 */
#ifndef IO_H_
#include <stdbool.h>
// Declarations

// In der von der Main Setup Methode aufgerufenen Funktion, werden die Ausgänge als Ausgänge konfiguriert (der Standardwert ist eine Konfiguration als Eingang). Eingänge werden die internen Pullup- Widerstände eingeschaltet und die Interrupts
void iosetup(void);
// Die Loop Methode gibt es nur der Vollständigkeit halber.
void ioloop(void);
// Setzt den Ausgang der LED auf den übergebenen Wert und gibt diesen zurück.
bool ioSetLed(bool);
// Gibt den aktuellen Zustand der LED zurück.
bool ioGetLed(void);
// Wenn die LED an den Taster gebunden wird, wird der Ausgang bei einem Taster Interrupt auf den selben Wert gesetzt. Dieses Binden kann mit der Funktion Ein- bzw. Ausgeschaltet werden.
void ioSetBindTasterToLed(bool);
// Mit dieser Funktion wird der Zustand des Binden der LED an den Taster abgefragt.
bool ioGetBindTasterToLed(void);
// Diese Funktion setzt die LED auf den Zustand des Tasters.
bool ioSetLedToTaster(void);
// Diese Funktion gibt den Zustand des Tasters zurück.
bool ioGetTaster(void);
// Wenn die LED Aus geschaltet ist, wird sie Ein geschaltet. Wenn die LED Ein geschaltet ist, wird sie Aus geschaltet.
bool ioToggleLed(void);


#define IO_H_
#endif /* IO_H_ */
