/*
 * helper.h
 *
 *  Created on: 05.06.2018
 *      Author: Fabian Spottog
 *              Hilfsfunktionen
 */

#ifndef HELPER_H_
#define HELPER_H_
// Wie bei der selben Funktion für den Linux PC werden hier die Bits innerhalb von einem Byte gedreht.
uint8_t ReverseByte(uint8_t);
// Hier wird jedes Bit in einem Byte gekippt, z.B.: aus 00110010 wird 11001101.
uint8_t ReverseAllBits(uint8_t);


//Preprozessor Helpers.
#define STR_HELPER(x) #x
#define STR(x) STR_HELPER(x)

#endif /* HELPER_H_ */
