/*
 * hallo.c
 *
 *  Created on: 23.03.2018
 *      Author: Fabian Spottog
 *      Mit Hilfe des Analog Digital Wandlers wird die analoge Spannung gemessen welche dem Helligkeitswert entspricht. Der im Attiny integrierte ADC wird im 10 Bit Free Run Modus mit einem Vorteiler von 8 betrieben. Das bedeutet, alle acht Taktzyklen wird automatisch eine Messung durchgeführt. Das sind 125.000 Messungen (125 KHz) in der Sekunde oder eine Messung dauert 87μs. Im Datenblatt wird eine Messdauer von 65-260μs empfohlen. Ist die Messdauer zu gering, kann es sein, dass die Sample & Hold Schaltung nicht schnell genug eingestellt hat. Ist die Messdauer zu lang, kann die Sample & Hold Schaltung die Spannung nicht lange genug halten (siehe Elektronik Grundlagen). Nach dem erfolgreichen Abschluss einer Messung wird diese in das ADC Register geschrieben. Von diesem Register aus liest die Software die Messung, sobald der Messwert angefordert wird. Eine ISR Routine oder eine Aktion in der Loop Funktion wird nicht benötigt. Die komplette Arbeit macht der ADC in Hardware.
 */
#include <avr/io.h>
#include <stdbool.h>
#include "light.h"
#include "speicher.h"

//Offset zur Berechnung der Helligkeit.
uint16_t Offset;
// Verstärkung zur Berechnung der Helligkeit.
uint16_t Span;
// Der Aufruf erfolgt aus der Main Methode. Hier wird der ADC konfiguriert, die Abgleichwerte aus dem EEPROM gelesen und auf Plausibilität geprüft.
void lightsetup() {
	 //Analog Comparator (vergleich von 2 Spannungen) Ausschalten, verbraucht nur Strom.
	//ACSR = 1 << ACD;
	// Pin Konfiguration für den ADC auswählen.
	// VCC Als AREF und PB5 als Eingang.
	//ADMUX = 0x00;
	ADMUX = (1<<ADLAR); // 8Bit Linksshift mode
	ADCSRB = 0;	//Free Running Mode
	// ADC Control and Status Register A
	// ADC einschalten (ADEN), MEssvorgang Starten (ADSC), FreeRUN Aktiviert(ADATE), Teilungsfaktor 64.
	// es sollte als Takt am ADC ein Wert zwischen 50-200 KHz anliegen.
	// bei 8 MHz und einen Teiler von 64 sind wir bei 125 KHz (ADPS...)
	// bei 16 MHz und einen Teiler von 128 sind wir bei 125 KHz (ADPS...)
	ADCSRA = (1 << ADEN) | (1<< ADSC) | (1<<ADATE) | (0<<ADPS2) | (1<ADPS1) | (1<ADPS0);	//Teiler 8
	//ADCSRA = (1 << ADEN) | (1<< ADSC) | (1<<ADATE) | (1<<ADPS2) | (1<ADPS1) | (0<ADPS0);	//Teiler 64
	//ADCSRA = (1 << ADEN) | (1<< ADSC) | (1<<ADATE) | (1<<ADPS2) | (1<ADPS1) | (1<ADPS0);	//Teiler 128
	//Offset und Span lesen
	Offset = speicherlightreadoffset();
	Span = speicherlightreadspan();
	// Wenn Span 0 dann auf 1 setzen.
	// So wird garantiert, dass gelesen werden kann.
	if(!Span) lightSetSpan(1);
}
// Die Loop Funktion ist nur zur Vollständigkeit implementiert.
void lightloop() {
	// nicht genutzt.
}
// Liest den zuletzt gemessenen Wert direkt aus dem ADC Register und gibt ihn zurück.
uint16_t	lightGetADCValue(){
	return ADC;
}
// Gibt den zuletzt gemessen Wert korrigiert um die Kalibrierungswerte zurück.
uint16_t lightGet(){
	return (lightGetADCValue() + Offset) * Span;
}
// Speichert den übergebenen Offsetwert und kopiert ihn ins EEPROM.
void lightSetOffset(uint16_t newOffset){
	Offset = newOffset;
	speicherlightwriteoffset(newOffset);
}
// Speichert den übergebenen Verstärkungsfaktor und kopiert ihn ins EEPROM
void lightSetSpan(uint16_t newSpan){
	Span = newSpan;
	speicherlightwritespan(newSpan);
}
// Gibt den aktuellen Offsetwert zurück.
uint16_t lightGetOffset(void){
	return Offset;
}
// Gibt den aktuellen Verstärkungsfaktor zurück.
uint16_t lightGetSpan(void){
	return Span;
}
