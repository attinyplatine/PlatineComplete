/*
 * hallo.c
 *
 *  Created on: 14.05.2018
 *      Author: Fabian Spottog
 *       Title: Platine.main
 *		Der Einstiegspunkt beim Start des Microcontrollers ist die main Funktion. Als erstes wird die setup Funktion aufgerufen, welche alle Ausgänge ausschaltet um einen definierten Zustand zu erhalten und anschließend die Setup Funktionen der einzelnen Unterprogramme aufruft.
 *		Nach dieser Initialisierung werden die Interrupts eingeschaltet und die Mainloop gestartet.
 *		Die Mainloop wird nie verlassen und ruft bei jedem Durchlauf die loop Funktion auf.
 *		Bei jedem 16384. Durchlauf wird zusätzlich die slowLoop Funktion aufgerufen. (16384 = 214)
 *		In der loop Funktion werden von allen einzelnen Unterprogrammen die jeweiligen eigenen Loop Funktionen aufgerufen. Was in den einzelnen Unterprogrammen passiert kann an den entsprechenden Stellen nachgelesen werden.
 *		Die slowLoop Funktion wird nur in der user.h verwendet und bietet dort die Möglichkeit in niedriger Frequenz eine Ausführung zu starten, wie in dem Beispiel Programm die Status Ausgabe über die Serielle Schnittstelle.
 */
#include <avr/io.h>
#include <avr/interrupt.h>	// wird für sei() Benötigt.
#include <stdbool.h>
#include "io.h"
#include "light.h"
#include "entf.h"
#include "uart.h"
#include "helper.h"
#include "user.h"
#include "receive.h"
// Declarations
//Sicherung, dass für den richtigen AVR programmiert wird, und die F_CPU wird ausgegeben.
#if ! defined(F_CPU)
	#error "NO F_CPU DEFINED!"
#else
	#pragma message "F_CPU is defined with: " STR(F_CPU)
#endif
#if ! defined(__AVR_ATtiny85__)
	#error "Wrong AVR Type!"
#endif
void setup(void);
void loop(void);
void slowLoop(void);
// Mainfunktion mit Dauerloop
int main(void){
    // Init
	setup();
	sei();
	//Main Loop
	uint16_t i =0;
	while(true){
       	//call Loop
       	loop();
       	//gelegentlich langsame Loop aufrufen.
       	if(i++ == 16384){ //(2^14)
       		i = 0;
       		slowLoop();
       	}
	}
	return 0;
}
// Init
void setup(void){
    // Register löschen
    DDRB = 0x00;
    PORTB = 0x00;
    //Programmmodule initialisieren.
    iosetup();
    lightsetup();
    entfsetup();
    uartsetup();
    receiveSetup();
    usersetup();
}
//wird alle x ms aufgerufen.
void loop(void){
	ioloop();
	lightloop();
	uartloop();
	userloop();
	entfloop();
}
void slowLoop(void){
	userslowloop();
}
